require 'rails_helper'

RSpec.describe IA, type: :model do
  include MovimentosHelper

  describe 'Min-max' do
    # context 'a IA só tem uma opção de movimento (com remoção) a fazer' do
    #   it 'deverá retornar o tabuleiro com a remoção feita' do
    #     tabuleiro = Tabuleiro.new(Cor::BRANCA)
    #     for i in 0..7
    #       for j in 0..7
    #         tabuleiro.matriz[i, j].peca = nil
    #       end
    #     end
    #     tabuleiro.matriz[0, 0].peca = Peao.new({:cor => Cor::BRANCA, :lado_inicial => Lado::BAIXO})
    #     tabuleiro.matriz[1, 1].peca = Peao.new({:cor => Cor::PRETA, :lado_inicial => Lado::CIMA})
    #     tabuleiro.matriz[7, 7].peca = Peao.new({:cor => Cor::PRETA, :lado_inicial => Lado::CIMA})
    #     ia = IA.new()
    #     ia.set_cor_de_suas_pecas(Cor::BRANCA)
    #     oraculo = ia.min_max_decision(tabuleiro)
    #   end
    #end


    # context 'a IA só tem duas opções de movimento (com uma remoção cada) a fazer' do
    #   it 'deverá retornar o tabuleiro com movimento feito' do
    #     tabuleiro = Tabuleiro.new(Cor::BRANCA)
    #     for i in 0..7
    #       for j in 0..7
    #         tabuleiro.matriz[i, j].peca = nil
    #       end
    #     end
    #     tabuleiro.matriz[0, 0].peca = Peao.new({:cor => Cor::BRANCA, :lado_inicial => Lado::BAIXO})
    #     tabuleiro.matriz[1, 1].peca = Peao.new({:cor => Cor::PRETA, :lado_inicial => Lado::CIMA})
    #     tabuleiro.matriz[1, 3].peca = Peao.new({:cor => Cor::PRETA, :lado_inicial => Lado::CIMA})
    #     tabuleiro.matriz[3, 3].peca = Peao.new({:cor => Cor::BRANCA, :lado_inicial => Lado::BAIXO})
    #     tabuleiro.matriz[4, 4].peca = Peao.new({:cor => Cor::PRETA, :lado_inicial => Lado::CIMA})
    #     tabuleiro.matriz[4, 6].peca = Peao.new({:cor => Cor::PRETA, :lado_inicial => Lado::CIMA})
    #     tabuleiro.matriz[6, 6].peca = Peao.new({:cor => Cor::PRETA, :lado_inicial => Lado::CIMA})
    #     movimentos = EstadosSucessores.new({:cor_das_pecas => Cor::BRANCA, :tabuleiro => tabuleiro}).movimento_com_mais_remocoes_possiveis
    #[  [M(3,3 para 5,5), M(5,5 para 4,6)], [M(3,3 para 5,5), M(5,5 para 3,7)] ]
    # movimentos = EstadosSucessores.new({:cor_das_pecas => Cor::BRANCA, :tabuleiro => tabuleiro}).movimento_com_mais_remocoes_possiveis
    # oraculo = ia.min_max_decision(tabuleiro)
    # expect(oraculo).to eq(true)
    # end
    # end
    # end
    #
    # context 'início do jogo' do
    #   it 'deverá retornar um tabuleiro com uma jogada feita' do
    #     tabuleiro = Tabuleiro.new(Cor::BRANCA)
    #     for i in 0..7
    #       for j in 0..7
    #         tabuleiro.matriz[i, j].peca = nil
    #       end
    #     end
    #     tabuleiro.matriz[0, 0].peca = Peao.new({:cor => Cor::BRANCA, :lado_inicial => Lado::BAIXO})
    #     tabuleiro.matriz[1, 1].peca = Peao.new({:cor => Cor::PRETA, :lado_inicial => Lado::CIMA})
    #     tabuleiro.matriz[1, 3].peca = Peao.new({:cor => Cor::PRETA, :lado_inicial => Lado::CIMA})
    #     tabuleiro.matriz[3, 3].peca = Peao.new({:cor => Cor::BRANCA, :lado_inicial => Lado::BAIXO})
    #     tabuleiro.matriz[4, 4].peca = Peao.new({:cor => Cor::PRETA, :lado_inicial => Lado::CIMA})
    #     tabuleiro.matriz[4, 6].peca = Peao.new({:cor => Cor::PRETA, :lado_inicial => Lado::CIMA})
    #     tabuleiro.matriz[6, 6].peca = Peao.new({:cor => Cor::PRETA, :lado_inicial => Lado::CIMA})
    #     ia = IA.new()
    #     ia.set_cor_de_suas_pecas(Cor::BRANCA)
    #     oraculo = ia.min_max_decision(tabuleiro)
    #   end
    # end

  end
end